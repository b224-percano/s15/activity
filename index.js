console.log("Hello World");
console.log(" ");

/*
	1. Create variables to store to the following user details:

	-first name - String
	-last name - String
	-age - Number
	-hobbies - Array
	-work address - Object

		-The hobbies array should contain at least 3 hobbies as Strings.
		-The work address object should contain the following key-value pairs:

			houseNumber: <value>
			street: <value>
			city: <value>
			state: <value>

	Log the values of each variable to follow/mimic the output.

	Note:
		-Name your own variables but follow the conventions and best practice in naming variables.
		-You may add your own values but keep the variable names and values Safe For Work.
*/

	//Add your variables and console log for objective 1 here:




/*			
	2. Debugging Practice - Identify and implement the best practices of creating and using variables 
	   by avoiding errors and debugging the following codes:

			-Log the values of each variable to follow/mimic the output.
*/	


	// Objective 1
	let firstName = "Aljohn";
	console.log("First Name: " + firstName);

	let lastName = "Percano";
	console.log("Last Name: " + lastName);

	let age1 = 25;
	console.log("Age: " + age1);
	console.log("Hobbies: ");

	let hobbies = ['Dancing', 'Playing Games', 'Coding'];
	console.log(hobbies);

	console.log("Work Address: ");
	let workAdd = {
		houseNumber: "6",
		street: "Nasugbu",
		city: "Batangas",
		state: "Philippines",
	};
	console.log(workAdd);


	// Objective 2
	let fullName = "Steve Rogers";
	console.log("My full name is: " + fullName);

	let age = 40;
	console.log("My current age is: " + age);
	
	let friends = ["Tony","Bruce","Thor","Natasha","Clint","Nick"];
	console.log("My Friends are: ")
	console.log(friends);

	console.log("My Full Profile: ")
	let profile = {
		userName: "captain_america",
		fullName: "Steve Rogers",
		age: 40,
		isActive: false,
	};
	console.log(profile);

	let fullName1 = "Bucky Barnes";
	console.log("My bestfriend is: " + fullName1);

	lastLocation = "Atlantic Ocean";
	console.log("I was found frozen in: " + lastLocation);

